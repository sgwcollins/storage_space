(function($) {
    "use strict";



    var inputs = document.querySelectorAll('.inputfile');

    Array.prototype.forEach.call(inputs, function(input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener('change', function(e) {
            console.log("test");
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();

            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });

        // Firefox bug fix
        input.addEventListener('focus', function() {
            input.classList.add('has-focus');
        });
        input.addEventListener('blur', function() {
            input.classList.remove('has-focus');
        });

    });
    /*
     * Customly Styled Select input field
     */
    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function(el) {
        new SelectFx(el);
    });

    /*
     * On Parallax for .parallax class
     */
    $('.parallax').parallax("50%", 0.2);

    /*
     * Custom Data Fixed
     */
    $('.beactive').addClass('active');
    $('.beactive').removeClass('beactive');





    /*
     * Room Search form Check in and out Datepicker
     */
    $('.input-group.mg-check-in').datepicker({
        startDate: "dateToday",
        autoclose: true
    });



    $('.input-group.mg-book-in').datepicker({
        startDate: $('.book-in').val(),
        endDate: $('.book-out').val(),
        autoclose: true
    });


    $('.input-group.mg-check-in').on('hide', function(e) {


        if (e.dates.length) {
            var strDate = e.date;
            strDate.setDate(strDate.getDate() + 1);

            // $('.mg-check-out').datepicker('clearDates');
            $('.mg-check-out').datepicker('setStartDate', strDate);
        }

        $(e.currentTarget).removeClass('focus');

    });

    $('.input-group.mg-check-in').on('show', function(e) {

        $(e.currentTarget).addClass('focus');

    });

    $('.input-group.mg-check-out').on('show', function(e) {

        $(e.currentTarget).addClass('focus');

    });

    $('.input-group.mg-check-out').on('hide', function(e) {

        $(e.currentTarget).removeClass('focus');

    });

    $('.input-group.mg-check-in').on('changeDate', function(e) {

        if (e.dates.length) {
            var inDate = e.date,
                outDate = $('.mg-check-out').datepicker('getDate');

            if (outDate && inDate >= outDate) {
                $('.mg-check-out').datepicker('clearDates');
            }

        } else {
            $('.mg-check-out').datepicker('clearDates');
        }
    });

    $('.input-group.mg-check-out').datepicker({
        startDate: "dateToday",
        autoclose: true
    });

    $('.input-group.mg-book-out').datepicker({
        startDate: $('.book-in').val(),
        endDate: $('.book-out').val(),
        autoclose: true
    });

    $('.input-group.mg-book-in').datepicker({
        startDate: $('.book-in').val(),
        endDate: $('.book-out').val(),
        autoclose: true
    });

    $('.input-group.mg-book-in').on('hide', function(e) {


        if (e.dates.length) {
            var strDate = e.date;
            strDate.setDate(strDate.getDate() + 1);

            // $('.mg-check-out').datepicker('clearDates');
            $('.mg-book-out').datepicker('setStartDate', strDate);
        }

        $(e.currentTarget).removeClass('focus');

    });

    $('.input-group.mg-book-in').on('show', function(e) {

        $(e.currentTarget).addClass('focus');

    });

    $('.input-group.mg-book-out').on('show', function(e) {

        $(e.currentTarget).addClass('focus');

    });

    $('.input-group.mg-book-out').on('hide', function(e) {

        $(e.currentTarget).removeClass('focus');

    });

    $('.input-group.mg-book-in').on('changeDate', function(e) {

        if (e.dates.length) {
            var inDate = e.date,
                outDate = $('.mg-book-out').datepicker('getDate');

            if (outDate && inDate >= outDate) {
                $('.mg-book-out').datepicker('clearDates');
            }

        } else {
            $('.mg-book-out').datepicker('clearDates');
        }
    });

    /*
     * Booking progress line JS
     */
    $('.mg-booking-form > ul > li:nth-child(1)').click(function(event) {



        if ($('.mg-booking-form > ul > li:nth-child(1)').hasClass('mg-step-done')) {
            $('.mg-booking-form > ul > li:nth-child(1)').removeClass('mg-step-done');
        }
        if ($('.mg-booking-form > ul > li:nth-child(2)').hasClass('mg-step-done')) {
            $('.mg-booking-form > ul > li:nth-child(2)').removeClass('mg-step-done');
        }
        if ($('.mg-booking-form > ul > li:nth-child(3)').hasClass('mg-step-done')) {
            $('.mg-booking-form > ul > li:nth-child(3)').removeClass('mg-step-done');
        }

        if ($('.mg-booking-form > ul > li:nth-child(4)').hasClass('mg-step-done')) {
            $('.mg-booking-form > ul > li:nth-child(4)').removeClass('mg-step-done');
        }

    });

    $('.mg-booking-form > ul > li:nth-child(2)').click(function() {

        if ($(this).hasClass('go-to-payment') == true) {

            

            var $myForm = $('#panel-1')


            if (!$myForm[0].checkValidity()) {

                // If the form is invalid, submit it. The form won't actually submit;
                // this will just cause the browser to display the native HTML5 error messages.

                $myForm.find(':submit').click();

            } else {


                // var formdData = $('#panel-1').serialize();
                // $.ajax({
                //     url: 'createPostDB',
                //     type: 'post',
                //     cache: false,
                //     data: formdData,
                //     dataType: 'json',
                //     success: function(data) {
                        

                //         var calculated_price = (17.5 / 100) * parseInt($('#slider-range-value').val());

                //         $('#hidden_listing_name').val($('#space-name').val());
                //         $('#hidden_listing_id').val(data);

                //         $('.space-title').text($('#space-name').val());
                //         $('.space-description').text($('#space-description').val());
                //         $('.type').text($('#type').val());
                //         $('.dimensions').text($('#dimensions').val());
                //         $('.address').text($('#address').val());
                //         $('.price').text("$ " + $('#slider-range-value').val());
                //         $('.day_price').text("$ " + calculated_price);
                //     },
                //     error: function(data) {
                //         console.log(data)
                //     }
                // });

                $(this).tab('show');

                $('html, body').animate({
                    scrollTop: $(".mg-booking-form").offset().top - 100
                }, 300);

                $('a[href="' + $(this).attr('href') + '"]').parents('li').trigger('click');
                $('.mg-booking-form > ul > li.active').removeClass('active');
                $('a[href="' + $(this).attr('href') + '"]').parents('li').addClass('active');

            }
        }


    });

    $('.mg-booking-form > ul > li:nth-child(3)').click(function() {
        $('.mg-booking-form > ul > li:nth-child(1)').addClass('mg-step-done');
        $('.mg-booking-form > ul > li:nth-child(2)').addClass('mg-step-done');

        if ($('.mg-booking-form > ul > li:nth-child(3)').hasClass('mg-step-done')) {
            $('.mg-booking-form > ul > li:nth-child(3)').removeClass('mg-step-done');
        }

        if ($('.mg-booking-form > ul > li:nth-child(4)').hasClass('mg-step-done')) {
            $('.mg-booking-form > ul > li:nth-child(4)').removeClass('mg-step-done');
        }
    });

    $('.mg-booking-form > ul > li:nth-child(4)').click(function() {
        $('.mg-booking-form > ul > li:nth-child(1)').addClass('mg-step-done');
        $('.mg-booking-form > ul > li:nth-child(2)').addClass('mg-step-done');
        $('.mg-booking-form > ul > li:nth-child(3)').addClass('mg-step-done');

        if ($('.mg-booking-form > ul > li:nth-child(4)').hasClass('mg-step-done')) {
            $('.mg-booking-form > ul > li:nth-child(4)').removeClass('mg-step-done');
        }
    });



    $('.btn-next-tab').click(function(e) {

        e.preventDefault();


        if ($(this).hasClass('go-to-payment') == true) {

            var $myForm = $('#panel-1')


            if (!$myForm[0].checkValidity()) {

                // If the form is invalid, submit it. The form won't actually submit;
                // this will just cause the browser to display the native HTML5 error messages.

                $myForm.find(':submit').click();

            } else {


                var calculated_price = ((17.5 / 100) * parseInt($('#slider-range-value').val())).toFixed(2);

                $('.space-title').text($('#space-name').val());
                $('.space-description').text($('#space-description').val());
                $('.type').text($('#type').val());
                $('.dimensions').text($('#dimensions').val());
                $('.address').text($('#address').val());
                $('.price').text("$ " + $('#slider-range-value').val());
                $('.day_price').text("$ " + calculated_price);

                $(this).tab('show');

                $('html, body').animate({
                    scrollTop: $(".mg-booking-form").offset().top - 100
                }, 300);

                $('a[href="' + $(this).attr('href') + '"]').parents('li').trigger('click');
                $('.mg-booking-form > ul > li.active').removeClass('active');
                $('a[href="' + $(this).attr('href') + '"]').parents('li').addClass('active');



            }
        } else if ($(this).hasClass('go-to-thank-you') == true) {


            var $myForm = $('#image-form')
            if (!$myForm[0].checkValidity()) {
                // If the form is invalid, submit it. The form won't actually submit;
                // this will just cause the browser to display the native HTML5 error messages.
                $myForm.find(':submit').click()

            } else {

                $(this).tab('show');

                $('html, body').animate({
                    scrollTop: $(".mg-booking-form").offset().top - 100
                }, 300);

                $('a[href="' + $(this).attr('href') + '"]').parents('li').trigger('click');
                $('.mg-booking-form > ul > li.active').removeClass('active');
                $('a[href="' + $(this).attr('href') + '"]').parents('li').addClass('active');

            }
        } else if ($(this).hasClass('go-main-page') == true) {

                var formdData = $('#panel-1').serialize();
                $.ajax({
                    url: 'createPostDB',
                    type: 'post',
                    cache: false,
                    data: formdData,
                    dataType: 'json',
                    success: function(data) {
                                           $('#hidden_listing_name').val($('#space-name').val());
                                             $('#hidden_listing_id').val(data);
                            $('#image-form').submit()
     
                    },
                    error: function(data) {
                        
                    }
                });
            
        } else if ($(this).hasClass('go-to-payment-details') == true) {


            var $myForm = $('.book-form')
            if (!$myForm[0].checkValidity()) {
                // If the form is invalid, submit it. The form won't actually submit;
                // this will just cause the browser to display the native HTML5 error messages.
                $myForm.find(':submit').click()

            } else {
                var date1 = new Date($('#exampleInputEmail1').val());
                var date2 = new Date($('#exampleInputEmail2').val());

                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                var week_price = parseFloat($('.price-week').text());
                var day_price = parseFloat($('.price').text());

                $('.days').text(diffDays);

                var days = parseInt(diffDays%7);
    			var weeks = parseInt(diffDays/7);

    			var total =  (days * day_price) + (weeks * week_price);

                $('.gross-total').text(parseFloat(total).toFixed(2));

                $('.total_days').val(parseFloat(diffDays).toFixed(2))
                $('.total_price').val(parseFloat(total).toFixed(2))


                $(this).tab('show');

                $('html, body').animate({
                    scrollTop: $(".mg-booking-form").offset().top - 100
                }, 300);

                $('a[href="' + $(this).attr('href') + '"]').parents('li').trigger('click');
                $('.mg-booking-form > ul > li.active').removeClass('active');
                $('a[href="' + $(this).attr('href') + '"]').parents('li').addClass('active');
            }


        } else if ($(this).hasClass('book-now') == true) {

        	$('.book-form').submit();

        }


    });

    $('.btn-prev-tab').click(function(e) {

        e.preventDefault();

        // console.log($($(this).attr('href')));
        $(this).tab('show');

        $('html, body').animate({
            scrollTop: $(".mg-booking-form").offset().top - 100
        }, 300);

        $('a[href="' + $(this).attr('href') + '"]').parents('li').trigger('click');
        $('.mg-booking-form > ul > li.active').removeClass('active');
        $('a[href="' + $(this).attr('href') + '"]').parents('li').addClass('active');
    });



    $('.agree').change(function() {

        if($(this).is(":checked")) {


            $('.submitapprove').removeClass('hide');
        
        }else{

             $('.submitapprove').addClass('hide');
        }

    }); 







    /*
     * Search box toggle at Header
     */
    $('.mg-search-box-trigger').click(function() {
        var sbox = $(this).next();

        // $(this).toggleClass('mg-sb-active');
        $(this).find('i').toggleClass('fa-times');
        sbox.toggleClass('mg-sb-active');

        return false;
    });
    $('.pay-now').click(function() {
        $('.book').submit();
    })


    /*
     * Main Menu dropdown at Hover
     */
    if ($(window).width() >= 768) {
        $('.dropdown').hover(
            function() {
                $(this).addClass('open');
            },
            function() {
                $(this).removeClass('open');
            }
        );
    }

    $(window).resize(function() {
        if ($(window).width() >= 768) {
            $('.dropdown').hover(
                function() {
                    $(this).addClass('open');
                },
                function() {
                    $(this).removeClass('open');
                }
            );
        }
    });

    /*
     * Owl Carousel for Testimonials
     */
    $("#mg-testimonial-slider").owlCarousel({
        navigation: true,
        singleItem: true,
        pagination: false,
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        transitionStyle: "backSlide"

    });

    /*
     * Owl Carousel for Client Logo (Small 3 items Only)
     */
    $("#mg-part-logos").owlCarousel({
        items: 3,
        itemsDesktop: [1199, 3],
        navigation: true,
        pagination: false,
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

    });

    /*
     * Owl Carousel for Client Logo (Full 5 items Only)
     */
    $("#mg-part-logos-full").owlCarousel({
        items: 5,
        navigation: true,
        pagination: false,
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

    });

    /*
     * Owl Carousel for Blog post
     */
    $(".mg-post-images-slider").owlCarousel({
        singleItem: true,
        navigation: true,
        pagination: false,
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

    });

    /*
     * Owl Carousel for Gallery
     */
    var sync1 = $("#mg-gallery");
    var sync2 = $("#mg-gallery-thumb");
    sync1.owlCarousel({
        navigation: true,
        singleItem: true,
        pagination: false,
        afterAction: syncPosition,
        navigationText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>'],

    });

    sync2.owlCarousel({
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 3],
        itemsMobile: [479, 3],
        navigation: false,
        pagination: false,
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        afterInit: function(el) {
            el.find(".owl-item").eq(0).addClass("synced");
        }

    });

    function syncPosition(el) {
        var current = this.currentItem;
        $("#mg-gallery-thumb")
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced")
        if ($("#mg-gallery-thumb").data("owlCarousel") !== undefined) {
            center(current)
        }
    }

    sync2.on("click", ".owl-item", function(e) {
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo", number);
    });

    function center(number) {
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for (var i in sync2visible) {
            if (num === sync2visible[i]) {
                var found = true;
            }
        }

        if (found === false) {
            if (num > sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", num - sync2visible.length + 2)
            } else {
                if (num - 1 === -1) {
                    num = 0;
                }
                sync2.trigger("owl.goTo", num);
            }
        } else if (num === sync2visible[sync2visible.length - 1]) {
            sync2.trigger("owl.goTo", sync2visible[1])
        } else if (num === sync2visible[0]) {
            sync2.trigger("owl.goTo", num - 1)
        }

    }





    // Sticky Header
    $(window).ready(function() {
        sticky_check(this);
    });
    $(window).scroll(function() {
        sticky_check(this);
    });

    $(window).resize(function() {
        sticky_check(this);
    });

    function sticky_check($this) {
        if ($(window).width() >= 767) {
            if ($($this).scrollTop() > 150) {
                if (!$('.sticky-on-fixed').length && !$('.header.sticky').hasClass('transp')) {
                    $('body').prepend('<div class="sticky-on-fixed" style="height:' + $('.header.sticky').height() + 'px"></div>');
                };

                $('.header.sticky').addClass("sticky-on");
            } else {
                $('.header.sticky').removeClass("sticky-on");

                $('.sticky-on-fixed').remove();
            }
        } else {
            $('.header.sticky').removeClass("sticky-on");
            $('.sticky-on-fixed').remove();
        }
    }




    $("input[ name='dimensions']").change(function(values) {

        if (values.currentTarget.value == "Large") {


            $('#price-facts').text("Based on market statistics, recommended price range is between $50.00 - $72.50$");

     
        } else if (values.currentTarget.value == "Medium") {

            $('#price-facts').text("Based on market statistics, recommended price range is between $32.00 - $48.50$");

        } else if (values.currentTarget.value == "Small") {

            $('#price-facts').text("Based on market statistics, recommended price range is between $11.00 - $31.50$");
    
        } else if (values.currentTarget.value == "XLarge") {

            $('#price-facts').text("Based on market statistics, recommended price range is $73+");
    
        }
    });

    /*
     * Single room review ratting
     */
    $('#mg-star-position').on('starrr:change', function(e, value) {
        $('#mg-star-position-input').val(value);
    });

    $('#mg-star-comfort').on('starrr:change', function(e, value) {
        $('#mg-star-comfort-input').val(value);
    });

    $('#mg-star-price').on('starrr:change', function(e, value) {
        $('#mg-star-price-input').val(value);
    });

    $('#mg-star-quality').on('starrr:change', function(e, value) {
        $('#mg-star-quality-input').val(value);
    });

    /*
     * Nivo Lightbox for gallery
     */
    $('.mg-gallery-item a').nivoLightbox({
        effect: 'fadeScale'
    });

    /*
     * Google map for contact form
     */
    if ($('#mg-map').length) {

        var map = new GMaps({
            el: '#mg-map',
            lat: -37.81792,
            lng: 144.96506,
            zoom: 17
        });

        map.addMarker({
            lat: -37.81792,
            lng: 144.96506,
            title: 'Map',
            infoWindow: {
                content: '<strong>Envato</strong><br>Level 13, 2 Elizabeth St, Melbourne<br>Victoria 3000 Australia'
            }
        });
    }

})(jQuery);

$(window).load(function() {
    /*
     * Gallery Filter with Shuffle
     */
    var $grid = $('#mg-grid'),
        $sizer = $grid.find('.shuffle__sizer'),
        $filterType = $('#mg-filter input[name="filter"]');

    $grid.shuffle({
        itemSelector: '.mg-gallery-item',
        sizer: $sizer
    });

    $grid.shuffle('shuffle', $('#mg-filter input[name="filter"]:checked').val());

    $('label.btn-main').removeClass('btn-main');
    $('input[name="filter"]:checked').parent().addClass('btn-main');

    $filterType.change(function(e) {
        var group = $('#mg-filter input[name="filter"]:checked').val();

        $grid.shuffle('shuffle', group);

        $('label.btn-main').removeClass('btn-main');
        $('input[name="filter"]:checked').parent().addClass('btn-main');
    });
});

/*
 * Preloader
 */
$(window).load(function() {
    $('.preloader').fadeOut("slow");
});


/*
 * Validation
 */

var password = document.getElementById("create_password"),
    confirm_password = document.getElementById("confirm_password");

function validatePassword() {
    if (password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
        confirm_password.setCustomValidity('');
    }
}

// password.onchange = validatePassword;
// confirm_password.onkeyup = validatePassword;

$('#login_form').submit(function(event) {
    event.preventDefault();

    console.log($('[name=\"login_token\"]').val());

    $.ajax({
        url: $('#site_url').val()+'login',
        type: 'post',
        cache: false,
        data: {
            'login_string': $('#username').val(),
            'login_pass': $('#password').val(),
            'login_token': $('[name=\"login_token\"]').val()
        },
        dataType: 'json',
    })
        .done(function(data) {
            console.log(data);
            if (data['is_error'] == 1) {
                $('.loginmodal-container .alert.login').fadeIn("fast");
                $('#username , #password').addClass('error_text');
            } else {
                location.reload();
            }
        })
        .fail(function(xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            alert(err.Message);
        })
        .always(function(data) {
            //console.log(data);
        });
});

$('#create_form').submit(function(event) {
    event.preventDefault();


    $.ajax({
        url: 'register',
        type: 'post',
        cache: false,
        data: {
            'email': $('#email').val(),
            'password': $('#confirm_password').val(),
            'login_token': $('[name=\"login_token\"]').val()
        },
        dataType: 'json',
    })
        .done(function(data) {

            if (data['is_error'] == 1) {

                $('#signup-modal .loginmodal-container .alert.register').fadeIn("fast").html(data['error_text']);
                $('#signup-modal #email ,.signup-modal #create_password').addClass('error_text');




            } else {

                $('#signup-modal').modal('toggle');
                document.getElementById("create_form").reset();
                $('#signup-modal #email ,.signup-modal #create_password').removeClass('error_text');
                $('#signup-modal .loginmodal-container .alert').hide();
                $('.creation').fadeIn('fast').delay(10000).fadeOut('slow');





            }

        })
        .fail(function(xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            alert(err.Message);
        })
        .always(function(data) {
            //console.log(data);
        });

});
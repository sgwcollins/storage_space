<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Stored Next Door</title>
		
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic|Playfair+Display:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
		<!-- Bootstrap -->
		<link href="<?php echo asset_url();?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo asset_url();?>/css/font-awesome.min.css" rel="stylesheet">

		<link href="<?php echo asset_url();?>/css/owl.carousel.css" rel="stylesheet">
		<link href="<?php echo asset_url();?>/css/owl.theme.css" rel="stylesheet">
		<link href="<?php echo asset_url();?>/css/owl.transitions.css" rel="stylesheet">
		<link href="<?php echo asset_url();?>/css/cs-select.css" rel="stylesheet">
		<link href="<?php echo asset_url();?>/css/bootstrap-datepicker3.min.css" rel="stylesheet">
		<link href="<?php echo asset_url();?>/css/freepik.hotels.css" rel="stylesheet">
		<link href="<?php echo asset_url();?>/css/style.css" rel="stylesheet">
		<link href="<?php echo asset_url();?>/css/dropzone.css" rel="stylesheet">
    <link href="<?php echo asset_url();?>/css/nouislider.css" rel="stylesheet">






		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="js/html5shiv.min.js"></script>
		<script src="js/respond.min.js"></script>
		<![endif]-->
		<script src="<?php echo asset_url();?>/js/modernizr.custom.min.js"></script>
	</head>
<body>

      <div class="preloader"></div>

      <header class="header transp sticky"> <!-- available class for header: .sticky .center-content .transp -->

      <nav class="navbar navbar-inverse">

        <div class="container">

          <!-- Brand and toggle get grouped for better mobile display -->

          <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">

              <span class="sr-only">Toggle navigation</span>

              <span class="icon-bar"></span>

              <span class="icon-bar"></span>

              <span class="icon-bar"></span>

            </button>

            <a class="navbar-brand" href="<?php echo site_url(); ?>"><img src="<?php echo asset_url();?>/images/logo.png" alt="logo"></a>

          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">

             <?php if($is_logged_in){ ?>

              <li><a ><?php print_r($auth_data->email); ?></a></li>  
              <li><a href="<?php echo site_url();?>logout">Logout</a></li> 
                  <li class="dropdown">
                   <a href="#">
                  <i class="fa fa-gear"></i>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url();?>user/management">Edit User Details</a></li>
                  <li><a href="<?php echo site_url();?>user/post">Edit Posts</a></li>
                  <li><a href="<?php echo site_url();?>user/booking">View Bookings</a></li>
                </ul>
              </li> 

              <?php } else { ?>


              <li><a href="#" data-toggle="modal" data-target="#signup-modal">Sign Up</a></li>

              <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a></li>

              <?php } ?>

            </ul>

          </div><!-- /.navbar-collapse -->


        </div><!-- /.container-fluid -->

      </nav>

    </header>
          <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
              <div class="modal-dialog">
                
                
              <div class="loginmodal-container">
              <div class="alert alert-danger login" style="display:none;" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                Email and password dont match
              </div>
                <h1>Login to Your Account</h1><br>
                <?php echo form_open( '', array( 'id'=>'login_form' ,'class' => 'std-form' ) ); ?>
        
                <input type="text" id="username" name="user" placeholder="Username" required="true">
                <input type="password" id="password" name="pass" placeholder="Password" required="true">
                <input type="submit" name="login" class="login loginmodal-submit btn btn-main btn-block" value="Login">

                <input type="hidden" id="max_allowed_attempts" value="<?php echo config_item('max_allowed_attempts'); ?>" />
                <input type="hidden" id="mins_on_hold" value="<?php echo ( config_item('seconds_on_hold') / 60 ); ?>" />

                <input type="hidden" id="site_url" value="<?php echo site_url(); ?>" />
                
                <?php echo form_close(); ?>
                
                <div class="login-help">
                <a href="#" data-toggle="modal" data-target="#signup-modal">Register</a>
                </div>
              </div>
            </div>
            </div>

            <div class="modal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
              <div class="modal-dialog">
                
                
              <div class="loginmodal-container">
              <div class="alert alert-danger register" style="display:none;" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                Enter valid information
              </div>
                <h1>Sign up for an account</h1><br>
                <?php echo form_open( 'examples/create_user', array( 'id'=>'create_form' ,'class' => 'std-form' ) ); ?>
        
                <input type="email" id="email" name="user" placeholder="Email" required="true">
                <input type="password" id="create_password" name="pass" placeholder="Password" required="true">
                <input type="password" id="confirm_password" name="pass" placeholder="Please Confirm Password" required="true">

                <input type="submit" name="login" class="login loginmodal-submit btn btn-main btn-block" value="Login">

                <input type="hidden" id="max_allowed_attempts" value="<?php echo config_item('max_allowed_attempts'); ?>" />
                <input type="hidden" id="mins_on_hold" value="<?php echo ( config_item('seconds_on_hold') / 60 ); ?>" />

                <input type="hidden" id="site_url" value="<?php echo site_url(); ?>" />
                
                <?php echo form_close(); ?>
                
                <div class="login-help">
                
                </div>
              </div>
            </div>
            </div>


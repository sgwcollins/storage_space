		<footer class="mg-footer">
			
			<div class="mg-copyright">
				<div class="container">
					<div class="row">
						
						<div class="col-md-6">
							<ul class="mg-footer-nav">
								<li><a href="<?php echo site_url(); ?>">Home</a></li>



							<?php if($is_logged_in){ ?>


				              <li><a ><?php print_r($auth_data->email); ?></a></li>  
				              <li><a href="<?php echo site_url();?>logout">Logout</a></li> 
				          

				              <?php } else { ?>

				              <li><a href="#" data-toggle="modal" data-target="#signup-modal">Sign Up</a></li>

				              <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a></li>

				              <?php } ?>

                      <li><a href="<?php echo site_url(); ?>faq">FAQ</a></li>
							</ul>
						</div>
						<div class="col-md-6">
							<p>&copy; 2015 <b>StoredNextDoor</b>. All rights reserved.</p>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="<?php echo asset_url();?>/js/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?php echo asset_url();?>/js/bootstrap.min.js"></script>
		
		<script src="<?php echo asset_url();?>/js/owl.carousel.min.js"></script>
		<script src="<?php echo asset_url();?>/js/jssor.slider.mini.js"></script>
		<script src="<?php echo asset_url();?>/js/classie.js"></script>
		<script src="<?php echo asset_url();?>/js/selectFx.js"></script>
		<script src="<?php echo asset_url();?>/js/bootstrap-datepicker.min.js"></script>
		<script src="<?php echo asset_url();?>/js/starrr.min.js"></script>
		<script src="<?php echo asset_url();?>/js/nivo-lightbox.min.js"></script>
		<script src="<?php echo asset_url();?>/js/jquery.shuffle.min.js"></script>
		<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
		<script src="<?php echo asset_url();?>/js/jquery.parallax-1.1.3.js"></script>
		<script src="<?php echo asset_url();?>/js/script.js"></script>

    
	</body>
</html>
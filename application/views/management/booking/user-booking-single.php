
    <div class="mg-page-title parallax">

      <div class="container">

        <div class="row">

          <div class="col-md-12">

            <h2>User Booking</h2>

          </div>

        </div>

      </div>

    </div>





    <div class="mg-page">

      <div class="container">

        <div class="row">

          <div class="col-md-12">

            <div class="mg-booking-form">

                  <div class="row">

                      <div class="col-md-7 col-md-offset-2">

                <div class="mg-cart-container mg-paid">
                  <aside class="mg-widget mt50" id="mg-room-cart">
                    <h2 class="mg-widget-title">Booking Details</h2>
                    <div class="mg-widget-cart">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="mg-cart-room">
                            <h3 class="space-title"><?php echo $listings->name; ?></h3>
                            <p class="space-description"><?php echo $listings->{'space-description'}; ?></p>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="mg-widget-cart-row">
                            <strong>Type</strong>
                            <span class="type"><?php echo $listings->type; ?></span>
                          </div>
                          <div class="mg-widget-cart-row">
                            <strong>Dimensions</strong>
                            <span class="dimensions"><?php echo $listings->dimension; ?></span>
                          </div>
                          <div class="mg-cart-address">
                            <strong>Your Address:</strong>
                            <address class="address">
                              <?php echo $listings->address; ?><br>
                              <?php echo $listings->city; ?> <?php echo "Canada"; ?>
                            </address>
                          </div>
                          <div class="mg-cart-total">
                            <strong>Base price per week:</strong>
                            $<span class="price"><?php echo $listings->price; ?></span>
                          </div>
                          <div class="mg-cart-total">
                            <strong>Price per day:</strong>
                            $<span class="price"><?php echo $listings->daily_price; ?></span>
                          </div>
                          <div class="mg-cart-total">
                            <strong>Total Days:</strong>
                            <span class="days"><?php echo $listings->total_days; ?></span>
                          </div>
                          <div class="mg-cart-total">
                            <strong>Total</strong>
                            $<span class="gross-total"><?php echo $listings->total_price; ?></span>
                          </div>
                          <a href="../" class="btn btn-info pull-left">Back</a>
                        </div>
                      </div>
                    </div>
                  </aside>
                </div>
                </div>
                </div>


     

                  

                </div>


     

              </div>



            </div>

          </div>

        </div>

      </div>

    </div>
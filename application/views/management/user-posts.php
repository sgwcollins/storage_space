
    <div class="mg-page-title parallax">

      <div class="container">

        <div class="row">

          <div class="col-md-12">

            <h2>Create Storage Space</h2>

            <p>Cogitavisse erant puerilis utrum efficiantur adhuc expeteretur.</p>

          </div>

        </div>

      </div>

    </div>





    <div class="mg-page">

      <div class="container">

        <div class="row">

          <div class="col-md-12">

            <div class="mg-booking-form">



              <ul class="nav nav-tabs" role="tablist">

                <li role="presentation" class="active">

                  <a class="top-nav" href="#personal-info" aria-controls="personal-info" role="tab" data-toggle="tab"><span class="mg-bs-tab-num">1</span><span class="mg-bs-bar"></span>Space Details</a>

                </li>

                <li role="presentation">

                  <a class="top-nav" href="#payment" aria-controls="payment" role="tab" data-toggle="tab"><span class="mg-bs-tab-num">2</span><span class="mg-bs-bar"></span>Photos</a>

                </li>

                <li role="presentation">

                  <a class="top-nav" href="#thank-you" aria-controls="thank-you" role="tab" data-toggle="tab"><span class="mg-bs-tab-num">3</span>Thank You</a>

                </li>

              </ul>



              <div class="tab-content">

              

                

                <div role="tabpanel" class="tab-pane fade in active" id="personal-info">

                  <form name="panel-1" id="panel-1">
                  <!-- <form method="post" action="createPostDB" name="panel-1" id="panel-1"> -->




                    <div class="row">

                      <div class="col-md-8 col-md-push-2">

                        <div class="mg-book-form-personal">

                          



                          <div class="row pb40">

                            <div class="col-md-9">

                          <h2 class="mg-sec-left-title">Spacing Details</h2>

                          <div class="row pb40">

                          <div class="col-md-12">

                              <div class="mg-book-form-input">

                                <label>Space Name</label>

                                <input type="text" name="space-name" id="space-name" required  />

                              </div>

                              <div class="mg-book-form-textarea">

                                <label>Space Description</label>

                                <textarea name="space-description" id="space-description" rows="4" cols="50">At w3schools.com you will learn how to make a website. We offer free tutorials in all web development technologies.</textarea>

                              </div>

                              <div class="col-md-6">

                                <div class="mg-book-form-input">

                                  <label>Dimenson Ranges</label> <br/>

                                    <input type="radio" id="dimensions" required="true" name="dimensions" value="Small">Small (20 - 77 sq.ft)<br>

                                    <input type="radio" id="dimensions" required="true" name="dimensions" value="Medium">Medium (78 - 150 sq.ft)<br>

                                    <input type="radio" id="dimensions" required="true" name="dimensions" value="Large">Large (151+ sq.ft)<br>

                                </div>

                              </div>

                              <div class="col-md-6">

                                <div class="mg-book-form-input">

                                  <label>Set Weekly Price</label>

                                  <input id="slider-range-value" type="text"  name="price" />

                                
                                   
                                   <div id="slider-range-value">
                                    
                                  </div>

                                </div>

                              </div>

                              <div class="mg-book-form-input">

                                <label>Type</label><br/>

                                  <input type="radio" id="type" name="type" value="Garage">Garage<br>

                                  <input type="radio" id="type" name="type" value="Private" checked>Private Room<br>

                                  <input type="radio" id="type" name="type" value="Shared">Shared Space<br>

                              </div>

                              <div class="mg-book-form-input">

                              <label>Availabiliy</label><br/>

                               <div clas="col-md-9"s>

                                <div class="mg-b-forms">

                                  <form>

                                    <div class="row">

                                      <div class="col-md-12 col-xs-6">

                                        <div class="input-group date mg-check-in">

                                          <div class="input-group-addon"><i class="fa fa-calendar"></i></div>

                                          <input type="text" name="from" class="form-control" id="exampleInputEmail1" placeholder="Check In">

                                        </div>

                                      </div>

                                      <div class="col-md-12 col-xs-6">

                                        <div class="input-group date mg-check-out">

                                          <div class="input-group-addon"><i class="fa fa-calendar"></i></div>

                                          <input type="text" name="to" class="form-control" id="exampleInputEmail1" placeholder="Check Out">

                                        </div>

                                      </div>



                                    </div>

                                 </form>

                                </div>

                              </div>

                              </div>

                          </div>                  

                          </div>

                              <h2 class="mg-sec-left-title">Address Details</h2>

                              <div class="mg-book-form-input">

                                <label>Address</label>

                                <input type="text" id="address" name="address" class="form-control" required>

                              </div>

                              <div class="mg-book-form-input">

                                <label>City</label>

                                <input type="text" name="city" class="form-control" required>

                              </div>

                              <div class="mg-book-form-input">

                                <label>Province</label>

                                <input type="text" name="province" class="form-control" required>

                              </div>

                              <div class="mg-book-form-input">

                                <label>Postal Code</label>

                                <input type="text" name="postal" class="form-control" required>

                                </div>



                            </div>

                            <div class="col-md-6">

                          

                              

                            </div>

                          </div>


                          

                          <input type="hidden" name="user_id" value=<?php print_r($auth_data->user_id); ?> >

                          <a href="#payment" class="go-to-payment btn btn-dark-main btn-next-tab pull-right">Next</a>
                          
                          <input type="submit" class="hide">

            

                        </div>

                      </div>

                  

                    </div>



                  </form>

                  

                </div>

                <div role="tabpanel" class="tab-pane fade" id="payment">

              

                 <form method="POST" enctype="multipart/form-data" id="image-form" action="<?php echo site_url(); ?>/post/upload">


                <div class="box">
                  <input type="file" required="true" name="file[]" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
                  <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>

                   

                   <input type="hidden" name="hidden_listing_name" id="hidden_listing_name" value="">

                   <input type="hidden" name="hidden_listing_id" id="hidden_listing_id" value="">

                   
                  <a href="#thank-you" class="go-to-thank-you btn btn-dark-main btn-next-tab pull-right">Next</a>
                  <input type="submit" class="hide" >

                 </form>



         

                </div>

                <div role="tabpanel" class="tab-pane fade" id="thank-you">

          

                  <div class="mg-cart-container mg-paid">

                    <aside class="mg-widget mt50" id="mg-room-cart">

                      <h2 class="mg-widget-title">Listing Details</h2>

                      <div class="mg-widget-cart">

                        <div class="row">

                          <div class="col-md-6">

                            <div class="mg-cart-room">

                              <h3 class="space-title">Super Delux</h3>

                              <p class="space-description">Verbis texit statua status suo quidque concordia. Octavio, ignavi, iudicante intereant accusamus vitiis primos ullum paranda zenonem. Censes cadere urbanitas texit dicebas maius tranquilli, foris morborum divinum que medium utilior crudelis affert, quaerendum refert sequimur atqui ullus d ornamenta consumeret ut divinum, concedo percurri elaborare.</p>

                            </div>

                          </div>

                          <div class="col-md-6">

                            <div class="mg-widget-cart-row">

                              <strong>Type</strong>

                              <span class="type"></span>

                            </div>

                            <div class="mg-widget-cart-row">

                              <strong>Dimension Type</strong>

                              <span class="dimensions"></span>

                            </div>

                            <div class="mg-cart-address">

                              <strong>Your Address:</strong>

                              <address class="address">

                                2 Elizabeth St, Melbourne<br>

                                Victoria 3000 Australia

                              </address>

                            </div>

                            <div class="mg-cart-total">

                              <strong>Base price per week:</strong>

                              <span class="price"></span>

                            </div>


                            <div class="mg-cart-total">

                              <strong>Base price per day:</strong>

                              <span class="day_price"></span>

                            </div>



                             <a href="#" class="go-main-page btn btn-main btn-next-tab pull-right">Confirm Details</a>

                          </div>

                        </div>

                      </div>

                    </aside>

                  </div>



                </div>

              </form>

              </div>



            </div>

          </div>

        </div>

      </div>

    </div>
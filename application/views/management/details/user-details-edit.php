

    <div class="mg-page-title parallax">

      <div class="container">

        <div class="row">

          <div class="col-md-12">

            <h2>Edit User Details</h2>

          </div>

        </div>

      </div>

    </div>





<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<div class="container">
  <div class="row">
    <div class="col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
       <div class="well profile">
            <div class="col-sm-12">
                <div class="col-xs-12 col-sm-8">
                  <form action="managementEditSubmit" method="post">
                    <p><strong>First Name: </strong> <input required="true" value="<?php echo $userdata->first_name ?>" type="text" name="first_name" placeholder="First Name"></p>
                    <p><strong>Last Name: </strong> <input required="true" value="<?php echo $userdata->last_name ?>"    type="text" name="last_name" placeholder="Last Name"></p>
                    <p><strong>Occupation: </strong> <input required="true" value="<?php echo $userdata->occupation ?>"    type="text" name="occupation" placeholder="Occupation"></p>
                    <p><strong>About: </strong> <input required="true" value="<?php echo $userdata->about ?>"   type="text" name="about" placeholder="About"> </p>
                    <p><strong>Hobbies: </strong> 
                    <textarea rows="4" name="hobbies" cols="50"><?php 
                    if($userdata->hobbies){
                      echo $userdata->hobbies;
                    } 
                    ?>
                    </textarea>
                         </p>

                        <a href="management">
                          <button class="btn btn-info btn-block"><span class="fa fa-user"></span> View Profile </button>
                        </a>

                    </form>
                
                
                </div>             
            </div>
            <br class="clear" />            
           
       </div>                 
    </div>

  </div>
<!-- Usage as a class -->

</div>


		<div class="preloader"></div>
		<header class="header transp sticky"> <!-- available class for header: .sticky .center-content .transp -->
			<nav class="navbar navbar-inverse">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php echo site_url(); ?>"><img src="<?php echo asset_url();?>/images/logo.png" alt="logo"></a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="register">Become a Host</a></li>
							<li><a href="register">Sign up</a></li>
							<li class="active"><a href="login">Login</a></li>	
						</ul>
					</div><!-- /.navbar-collapse -->
					<div class="mg-search-box-cont pull-right">
						<a href="#" class="mg-search-box-trigger"><i class="fa fa-search"></i></a>
						<div class="mg-search-box">
							<form>
								<input type="text" name="s" class="form-control" placeholder="Type Keyword...">
								<button type="submit" class="btn btn-main"><i class="fa fa-search"></i></button>
							</form>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</nav>
		</header>
		
		<div class="mg-about-features">
			<div class="container">
				<div class="row">
					<div class="col-md-6 login-container">
						<?php
defined('BASEPATH') OR exit('No direct script access allowed');



if( ! isset( $on_hold_message ) )
{
	if( isset( $login_error_mesg ) )
	{
		echo '
			<div class="alert alert-danger error" role="alert">
				<p>
					Login Error #' . $this->authentication->login_errors_count . '/' . config_item('max_allowed_attempts') . ': Invalid Username, Email Address, or Password.
				</p>
				<p>
					Username, email address and password are all case sensitive.
				</p>
			</div>
		';
	}

	if( $this->input->get('logout') )
	{
		echo '
			<div class="alert alert-success" role="alert">
				<p>
					You have successfully logged out.
				</p>
			</div>
		';
	}

	echo form_open( $login_url, array( 'class' => 'std-form' ) ); 
?>

	<div class="col-md-3 login-container">

		<label for="login_string" class="form_label">Username</label>
		<input type="text" name="login_string" id="login_string" class="form_input" autocomplete="off" maxlength="255" />

		<br class="clear" />
		

		<label for="login_pass" class="form_label">Password</label>
		<input type="password" name="login_pass" id="login_pass" class="form_input password" maxlength="<?php echo config_item('max_chars_for_password'); ?>" autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" />

		<br class="clear" />


		<?php
			if( config_item('allow_remember_me') )
			{
		?>

			<br />

			<label for="remember_me" class="form_label">Remember Me</label>
			<input type="checkbox" id="remember_me" name="remember_me" value="yes" />

		<?php
			}
		?>

		<p>
			<?php
				$link_protocol = USE_SSL ? 'https' : NULL;
			?>
			<a href="<?php echo site_url('examples/recover', $link_protocol); ?>">
				Can't access your account?
			</a>
		</p>


		<input type="submit" class="btn btn-main btn-block" name="submit" value="Login" id="submit_button"  />


	</div>
</form>

<?php

	}
	else
	{
		// EXCESSIVE LOGIN ATTEMPTS ERROR MESSAGE
		echo '
			<div class="alert alert-danger exceed" role="alert">
				<p>
					Excessive Login Attempts
				</p>
				<p>
					You have exceeded the maximum number of failed login<br />
					attempts that this website will allow.
				<p>
				<p>
					Your access to login and account recovery has been blocked for ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes.
				</p>
				<p>
					Please use the <a href="/examples/recover">Account Recovery</a> after ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes has passed,<br />
					or contact us if you require assistance gaining access to your account.
				</p>
			</div>
		';
	}
	
	?>
					<br class="clear" />
					</div>

				
				
				
				</div>
			</div>
		</div>




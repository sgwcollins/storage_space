

     <div class="mg-page-title parallax">

      <div class="container">

        <div class="row">

          <div class="col-md-12">

            <h2>Frequently Asked Question</h2>


          </div>

        </div>

      </div>

    </div>

    <div class="mg-booking-form">
      <div class="container">
           <h3>Frequently asked questions</h3>
           <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          What is Storednextdoor?
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        Storednextdoor is a platform which connects people who want to store their belongings (Guests) with people providing their property for storage (Guardians).
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Does Storednextdoor own any of the listed storage spaces?
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        No, Storednextdoor does not own any of the listed storage spaces. Listed storage spaces are owned and under the responsibility of the respective Guardian.
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Does Storednextdoor have a corporate building of sorts?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        No, Storednextdoor is strictly an online company which enables the connection between Guests
        and Guardians.
      </div>
    </div>
  </div>
    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          Does Storednextdoor warrant or insure the properties of Guardians and/or Guests?
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
      <div class="panel-body">
        No, Storednextdoor does not warrant or insure any properties of the Guests and Guardians. If an incident arises it is to be handled between the Guest and Guardian and Storednextdoor is not party to it.
      </div>
    </div>
  </div>

  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFive">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          What are the storage sizes?
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
      <div class="panel-body">
       Storage spaces listed by Guardians fall into one of the four following categories:<br/>

          Small: 20 &#8594; 77 (square feet)<br/>

          Medium: 78 &#8594; 150 (square feet)<br/>

          Large: 151 &#8594; 300 (square feet)<br/>

          X-Large: 301 + (square feet)
      </div>
    </div>
  </div>

    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingSix">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
          How is cost calculated?
        </a>
      </h4>
    </div>
    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
      <div class="panel-body">
        Excluding taxes, cost is determined by the Guardian setting a weekly rate of their choice and from that Storednextdoor sets a daily rate which is equal to 17.5% of the set weekly rate. For
        example, a weekly rate of $100 would equal to a $17.50 daily rate.<br/>

        Going on from the above example, if a listing is booked for less than a week then the total cost

        equals $17.50 x (# of days booked).<br/>

        Furthermore if a listing is booked for more than a week, say 2 weeks and 3 days then the total

        price equals $200 + ($17.50 x 3).
      </div>
    </div>
  </div>

    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingSeven">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
          What items can a Guest not store?
        </a>
      </h4>
    </div>
    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
      <div class="panel-body">
A guest may not store any food, beverages, explosives, weapons, inflammable material or

dangerous, illegal, hazardous, or noxious substances or any animal or plants, noise-creating

chattels or any similar products. The Guest agrees that no business will be conducted through use

of the Company´s Storage Services, and that no item will be stored with the Company’s Storage

Services which would violate any law or ordinance now or hereafter in force.
      </div>
    </div>
  </div>

      <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingEight">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
          What properties can a Guardian list for storage space?
        </a>
      </h4>
    </div>
    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
      <div class="panel-body">
    A Guardian can list any property above 20 sq ft in size and that he or she is the legal owner or in

    lawful possession of. Furthermore listed storage spaces must be safe, clean and well kept.<br/>

    For any additional inquiries please contact us at connect@storednextdoor.com, our goal is to

    reply to all emails within 24 hours.
      </div>
    </div>
  </div>

</div>

    </div>    
    </div>







				<div class="alert alert-success creation" style="display:none;" role="alert">
							  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							  
							  Account Creation was successful. Please try and login with your credentials
							  
							</div>


		<div id="mega-slider" class="carousel slide " data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#mega-slider" data-slide-to="0" class="active"></li>
				<li data-target="#mega-slider" data-slide-to="1"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active beactive">
					<img src="<?php echo asset_url();?>/images/hero-image-1.jpg" alt="...">
					<div class="overlay"></div>
					<div class="carousel-caption">
						<h2>A Home For Your Thing </h2>
						<p>THE EVOLUTION OF STORAGE</p>
						<div class="row">
							<div class="col-md-3 btn-container">
							<a href="post/list">
								<button type="submit" class="btn btn-main btn-block">View Listings</button>
							</a>
							</div>
							<br class="clear"/>
						</div>
					</div>
				</div>
				<div class="item">	
					<img src="<?php echo asset_url();?>/images/hero-image-2.jpg" alt="...">
					<div class="overlay"></div>
					<div class="carousel-caption">
						<h2>Your Things At Your Convience</h2>
						<p>LIFE SIMPLIFIED</p>
							<div class="row">
								<div class="col-md-3 btn-container">
								<a href="post/list">
									<button type="submit" class="btn btn-main btn-block">View Listings</button>
								</a>
								</div>
								<br class="clear"/>
							</div>
						</div>
					</div>
				</div>
	

			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#mega-slider" role="button" data-slide="prev">
			</a>
			<a class="right carousel-control" href="#mega-slider" role="button" data-slide="next">
			</a>
		</div>

		<div class="mg-book-now">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<h2 class="mg-bn-title">Search Spaces <span class="mg-bn-big">For rates & availability</span></h2>
					</div>
					<div class="col-md-9">
						<div class="mg-bn-forms">
				      <form action="<?php echo site_url(); ?>post/list/search" method="POST" >

                                <div class="row">

                                  <div class="col-md-3 col-xs-6">

                                    <div class="input-group date mg-check-in">

                                      <div class="input-group-addon"><i class="fa fa-calendar"></i></div>

                                      <input type="text" name="from" class="form-control" id="exampleInputEmail1" placeholder="Check In">

                                    </div>

                                  </div>

                                  <div class="col-md-3 col-xs-6">

                                    <div class="input-group date mg-check-out">

                                      <div class="input-group-addon"><i class="fa fa-calendar"></i></div>

                                      <input type="text" name="to" class="form-control" id="exampleInputEmail1" placeholder="Check Out">

                                    </div>

                                  </div>

                                  <div class="col-md-3">

                                    <input type="text" name="location" class="form-control" id="location" placeholder="Location">


                                  </div>

                                  <div class="col-md-3">

                                    <button type="submit" class="btn btn-main btn-block">Check Now</button>

                                  </div>

                                </div>

                              </form>
						</div>
					</div>
				</div>
			</div>
		</div>


			<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			    	  <div class="modal-dialog">
			    	  	
			    	  	
							<div class="loginmodal-container">
							<div class="alert alert-danger" style="display:none;" role="alert">
							  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							  <span class="sr-only">Error:</span>
							  Enter a valid information
							</div>
								<h1>Login to Your Account</h1><br>
								<?php echo form_open( 'examples/ajax_attempt_login', array( 'id'=>'login_form' ,'class' => 'std-form' ) ); ?>
				
								<input type="text" id="username" name="user" placeholder="Username" required="true">
								<input type="password" id="password" name="pass" placeholder="Password" required="true">
								<input type="submit" name="login" class="login loginmodal-submit btn btn-main btn-block" value="Login">

								<input type="hidden" id="max_allowed_attempts" value="<?php echo config_item('max_allowed_attempts'); ?>" />
								<input type="hidden" id="mins_on_hold" value="<?php echo ( config_item('seconds_on_hold') / 60 ); ?>" />

								<input type="hidden" id="site_url" value="<?php echo site_url(); ?>" />
							  
							  <?php echo form_close(); ?>
								
							  <div class="login-help">
								<a href="#">Register</a> - <a href="#">Forgot Password</a>
							  </div>
							</div>
						</div>
					  </div>

					  <div class="modal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			    	  <div class="modal-dialog">
			    	  	
			    	  	
							<div class="loginmodal-container">
							<div class="alert alert-danger" style="display:none;" role="alert">
							  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							  <span class="sr-only">Error:</span>
							  Enter a valid information
							</div>
								<h1>Sign up for an account</h1><br>
								<?php echo form_open( 'examples/create_user', array( 'id'=>'create_form' ,'class' => 'std-form' ) ); ?>
				
								<input type="email" id="email" name="user" placeholder="Email" required="true">
								<input type="password" id="create_password" name="pass" placeholder="Password" required="true">
								<input type="password" id="confirm_password" name="pass" placeholder="Please Confirm Password" required="true">

								<input type="submit" name="login" class="login loginmodal-submit btn btn-main btn-block" value="Login">

								<input type="hidden" id="max_allowed_attempts" value="<?php echo config_item('max_allowed_attempts'); ?>" />
								<input type="hidden" id="mins_on_hold" value="<?php echo ( config_item('seconds_on_hold') / 60 ); ?>" />

								<input type="hidden" id="site_url" value="<?php echo site_url(); ?>" />
							  
							  <?php echo form_close(); ?>
								
							  <div class="login-help">
								<a href="#">Register</a> - <a href="#">Forgot Password</a>
							  </div>
							</div>
						</div>
					  </div>

		<div class="mg-about">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-push-1">
						<h2 class="mg-sec-left-title">About StoredNextDoor</h2>
						<p>At Storednextdoor we believe in challenging the status quo. We believe in thinking differently and in simplifying life. We do this by offering local and convenient storage space to people from all walks of life.</p>
						<p>Storednextdoor connects you with people who are offering storage space at locations which meet your every accommodation. From college students to frequent movers to busy works and to whomever, Storednextdoor welcomes you. With stellar customer service coupled with a growing and vibrant community, Storednextdoor is the easiest way for people to take advantage of their extra space.</p>
					</div>
		
				</div>
			</div>
		</div>

		<div class="mg-about promotion ">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-push-1 start-here">
						<h1 class="pull-left">Try it now , First 90 days Commission Free</h1>
						<button type="submit" class="btn btn-main start" data-toggle="modal" data-target="#signup-modal">Start Here</button>
							
					</div>
		
				</div>
			</div>
		</div>



	
		<div class="mg-testi-partners parallax">
				<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-push-1">
					<h2 class="mg-sec-left-title">What people are saying about us</h2>
						<div class="mg-testimonial-slider" id="mg-testimonial-slider">
							<?php
								foreach ($testmonials as $testmonials) {
							
								
							?>
							<blockquote>
								<div class="image"><img src="<?php echo site_url().$testmonials['image'];?>">
								</div>
								<div class="short_description">"<?php echo $testmonials['short_description'];?>"</div>
								<div class="full_name"> <?php echo $testmonials['full_name'];?> </div>
								<div class="status"> <?php echo $testmonials['status'];?> </div>
								<div class="btn-read-more">
									<a href="<?php echo 'testimonals/'.$testmonials['id'];?> "<button type="submit" class="btn btn-main btn-block">Read More</button></a>
							    </div>
								
								
							</blockquote>

							<?php
								
								}
							
								
							?>
					
						</div>
					</div>
				

				</div>
			</div>
		</div>

	
	





    <div class="mg-page-title parallax">

      <div class="container">

        <div class="row">

          <div class="col-md-12">

            <h2><?php echo $listings[0]['name']; ?></h2>

            

          </div>

        </div>

      </div>

    </div>

    <div class="mg-single-room-price">

      <div class="mg-srp-inner">$<?php echo $listings[0]['price']; ?><span>/Week</span></div>

    </div>

    <div class="mg-single-room">

      <div class="container">

        <div class="row">

          <div class="col-md-7">

            <div class="mg-gallery-container">

              <ul class="mg-gallery" id="mg-gallery">

              <?php

              foreach ($listings as $key => $value) {

              ?>

                <li><img src="<?php echo $value['image_url'] ?>" alt="Partner Logo"></li>

              <?php }?>

              </ul>

           

            </div>

          </div>

          <div class="col-md-5 mg-room-fecilities">

            <h2 class="mg-sec-left-title">Room Fecilities</h2>

            <div class="row">

              <div class="col-xs-6">

                <ul>

                  <li><i class="fp-ht-food"></i> Breakfast</li>

                  <li><i class="fa fa-sun-o"></i> Air conditioning</li>

                  <li><i class="fp-ht-parking"></i> Free Parking</li>

                  <li><i class="fp-ht-elevator"></i> Elevator</li>

                  <li><i class="fp-ht-maid"></i> Room service</li>

                </ul>

              </div>

              <div class="col-xs-6">

                <ul>

                  <li><i class="fp-ht-dumbbell"></i> GYM fecility</li>

                  <li><i class="fp-ht-tv"></i> TV LCD</li>

                  <li><i class="fp-ht-computer"></i> Wi-fi service</li>

                  <li><i class="fp-ht-bed"></i> 2 King Beds</li>

                  <li><i class="fp-ht-swimmingpool"></i> Swimming Pool</li>

                </ul>

              </div>

            </div>

          </div>

        </div>

        <div class="row">

          <div class="col-md-12">

            <div class="mg-single-room-txt">

              <h2 class="mg-sec-left-title">Room Description</h2>

              <p><?php echo $listings[0]['space-description']; ?> </p>

            </div>

          </div>

        </div>

      



        

      </div>

    </div>

 


  </body>

</html>
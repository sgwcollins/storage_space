

     <div class="mg-page-title parallax">

      <div class="container">

        <div class="row">

          <div class="col-md-12">

            <h2>Booking</h2>


          </div>

        </div>

      </div>

    </div>

    <div class="mg-booking-form">
           
              <ul class="nav nav-tabs" role="tablist">

                <li role="presentation" class="active">

                  <a href="#personal-info" aria-controls="personal-info" role="tab" data-toggle="tab"><span class="mg-bs-tab-num">1</span><span class="mg-bs-bar"></span>Book Date</a>

                </li>

                <li role="presentation">

                  <a href="#payment" aria-controls="payment" role="tab" data-toggle="tab"><span class="mg-bs-tab-num">2</span><span class="mg-bs-bar"></span>Pay Details</a>

                </li>

                <li role="presentation">

                  <a href="#book" aria-controls="book" role="tab" data-toggle="tab"><span class="mg-bs-tab-num">3</span>Pay</a>

                </li>

              </ul>

                    <div class="tab-content">
              
              
              <div role="tabpanel" class="tab-pane fade in active" id="personal-info">

                 <div class="mg-saerch-room">

                    <div class="mg-book-now">

                        <div class="row">

                          <div class="col-md-7 col-md-offset-3">

                            <div class="mg-bn-forms">

                             <input type="hidden" name="book-in" class="book-in" value="<?php echo date('m/d/Y',strtotime($listings->from)); ?>">
                             <input type="hidden" name="book-out" class="book-out" value="<?php echo date('m/d/Y',strtotime($listings->to)); ?>">

                              <form action="<?php echo site_url()?>post/book" class="book-form" method="POST" >

                              <input type="hidden" name="total_days" class="total_days" value="">
                              <input type="hidden" name="total_price" class="total_price" value="">


                                <div class="row">

                                  <div class="col-md-3 col-xs-6">
                            
                              

                                   <input type="hidden" name="listing_id" class="listing_id" value="<?php echo $listings->listing_id ?>">

                                   <input type="hidden" name="user_id" value="<?php echo $auth_data->user_id ?>">


                                    <div class="input-group date mg-book-in">

                                      <div class="input-group-addon"><i class="fa fa-calendar"></i></div>

                                      <input required="true" type="text"  name="book-in" class="form-control" id="exampleInputEmail1" placeholder="Check In">

                                    </div>

                                  </div>

                                  <div class="col-md-3 col-xs-6">

                                    <div class="input-group date mg-book-out">

                                      <div class="input-group-addon"><i class="fa fa-calendar"></i></div>

                                      <input required="true" type="text" name="book-out" class="form-control" id="exampleInputEmail2" placeholder="Check Out">

                                    </div>

                                  </div>

                             
                                  <div class="col-md-3">
                                  <input type="submit" class="hide">
                                    <a href="#payment" class="btn btn-main btn-block go-to-payment-details  btn-next-tab ">Next</a>
                                  </div>

                            
                                </div>

                              </form>

                            </div>

                          </div>

                        </div>

                    </div>

                  </div>
               
                
              </div>
              <div role="tabpanel" class="tab-pane fade" id="payment">
                
                        <div class="row">

                          <div class="col-md-7 col-md-offset-2">

                <div class="mg-cart-container mg-paid">
                  <aside class="mg-widget mt50" id="mg-room-cart">
                    <h2 class="mg-widget-title">Booking Details</h2>
                    <div class="mg-widget-cart">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="mg-cart-room">
                            <h3 class="space-title"><?php echo $listings->name; ?></h3>
                            <p class="space-description"><?php echo $listings->{'space-description'}; ?></p>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="mg-widget-cart-row">
                            <strong>Type</strong>
                            <span class="type"><?php echo $listings->type; ?></span>
                          </div>
                          <div class="mg-widget-cart-row">
                            <strong>Dimensions</strong>
                            <span class="dimensions"><?php echo $listings->dimension; ?></span>
                          </div>
                          <div class="mg-cart-address">
                            <strong>Address:</strong>
                            <address class="address">
                              <?php echo $listings->address; ?><br>
                              <?php echo $listings->city; ?> <?php echo "Canada"; ?>
                            </address>
                          </div>
                          <div class="mg-cart-total">
                            <strong>Base price per week:</strong>
                            $<span class="price-week"><?php echo $listings->price; ?></span>
                          </div>
                          <div class="mg-cart-total">
                            <strong>Price per day:</strong>
                            $<span class="price"><?php echo $listings->daily_price; ?></span>
                          </div>
                          <div class="mg-cart-total">
                            <strong>Total Days:</strong>
                            <span class="days">$<?php echo $listings->price; ?></span>
                          </div>
                          <div class="mg-cart-total">
                            <strong>Total</strong>
                            $<span class="gross-total"><?php echo $listings->price; ?></span>
                          </div>
                          <a href="#pay" class="btn btn-info btn-next-tab pull-right">Next</a>
                        </div>
                      </div>
                    </div>
                  </aside>
                </div>
                </div>
                </div>
                
             
                
              </div>
              <div role="tabpanel" class="tab-pane fade" id="book">
                  <div class="row">

                          <div class="col-md-7 col-md-offset-2">
            <h2 class="mg-sec-left-title">Card Info</h2>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="mg-book-form-input">
                              <label>Card Number</label>
                              <input type="text" class="form-control">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="mg-book-form-input">
                              <label>CVV</label>
                              <input type="password" class="form-control">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="mg-book-form-input">
                              <label>Expire</label>
                              <div class="row">
                                <div class="col-md-6">
                                  <select class="form-control">
                                    <option value="">Month</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                  </select>
                                </div>
                                <div class="col-md-6">
                                  <select class="form-control">
                                    <option value="">Year</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <a href="#payment" class="btn btn-main btn-block btn-next-tab book-now">Book now</a>

              </div>
              </div></div>
              
            </form>
          </div>





<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonal extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Testimonial_modal');
	}

	public function index()
	{

		print_r($this->Testimonial_modal->getAllTestimonals());

		
	}
	public function single($id)
	{
    $this->setup_login_form();  
		$data['testimonal'] = $this->Testimonial_modal->getTestimonalsById($id);

		$this->load->view('include/header.php');
    $this->load->view('testimonals/single.php',$data);
    $this->load->view('include/footer.php');
	}

    /**
   * Show any login error message.
   *
   * @param  bool  if login is optional or required
   */
  protected function setup_login_form( $optional_login = FALSE )
  {

    $this->is_logged_in();

    if( $this->auth_data ){
      $view_data['is_logged_in'] = TRUE;
      $view_data['auth_data'] = $this->auth_data;
    }else
      $view_data['is_logged_in'] = FALSE;


    $this->tokens->name = 'login_token';

    /**
     * Check if IP, username, or email address on hold.
     *
     * If a malicious form post set the on_hold authentication class 
     * member to TRUE, there'd be no reason to continue. Keep in mind that 
     * since an IP address may legitimately change, we shouldn't do anything 
     * drastic unless this happens more than an acceptable amount of times.
     * See the 'deny_access_at' config setting in config/authentication.php
     */
    if( $this->authentication->on_hold === TRUE )
    {
      $view_data['on_hold_message'] = 1;
    }

    // This check for on hold is for normal login attempts
    else if( $on_hold = $this->authentication->current_hold_status() )
    {
      $view_data['on_hold_message'] = 1;
    }


    // Display a login error message if there was a form post
    if( $this->authentication->login_error === TRUE )
    {
      // Display a failed login attempt message
      $view_data['login_error_mesg'] = 1;
    }

    // Redirect to specified page
    $redirect = $this->input->get('redirect')
      ? '?redirect=' . $this->input->get('redirect') 
      : '?redirect=' . config_item('default_login_redirect');

    // If optional login, redirect to optional login's page
    if( $optional_login )
    {
      $redirect = '?redirect=' . urlencode( $this->uri->uri_string() );

      $view_data['optional_login'] = TRUE;
    }

    // Set the link protocol
    $link_protocol = USE_SSL ? 'https' : NULL;

    // Load URL helper for site_url function
    $this->load->helper('url');

    // Set the login URL
    $view_data['login_url'] = site_url( LOGIN_PAGE . $redirect, $link_protocol );

    $this->load->vars( $view_data );
  }


}

/* End of file  */
/* Location: ./application/controllers/ */
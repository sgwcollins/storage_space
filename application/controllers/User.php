<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('Listing_Model');

		// Form and URL helpers always loaded (just for convenience)
        $this->load->helper('url');
        $this->load->helper('form');
	}

    public function login()
    {
        // Method should not be directly accessible

        // if( strtolower( $_SERVER['REQUEST_METHOD'] ) == 'post' ){


            
            print_r($this->ajax_attempt_login());
            

            
        // }

    }

    // --------------------------------------------------------------

    /**
     * Log out
     */
    public function logout()
    {
        $this->authentication->logout();

        // Set redirect protocol
        $redirect_protocol = USE_SSL ? 'https' : NULL;
        redirect('/');
        
    }



        /**
     * Test for login via ajax
     */
    public function ajax_attempt_login()
    {

        $this->is_logged_in();

        $this->tokens->name = 'login_token';

         if( $this->authentication->on_hold === TRUE )
        {
            $data['on_hold_message'] = 1;
        }

        // This check for on hold is for normal login attempts
        else if( $on_hold = $this->authentication->current_hold_status() )
        {
            $data['on_hold_message'] = 1;
        }
        

        if( $this->input->is_ajax_request() )
        {
            // Allow this page to be an accepted login page
            $this->config->set_item('allowed_pages_for_login', array('login') );

            // Make sure we aren't redirecting after a successful login
            $this->authentication->redirect_after_login = FALSE;

            // Do the login attempt
            $this->auth_data = $this->authentication->user_status( 0 );

            // Set user variables if successful login
            if( $this->auth_data )
                $this->_set_user_variables();


            // Call the post auth hook
            $this->post_auth_hook();

            // Login attempt was successful
            if( $this->auth_data )
            {
                echo json_encode(array(
                    'status'   => 1,
                    'user_id'  => $this->auth_user_id,
                    'username' => $this->auth_username,
                    'level'    => $this->auth_level,
                    'role'     => $this->auth_role,
                    'email'    => $this->auth_email
                ));
            }

            // Login attempt not successful
            else
            {


                $on_hold = ( 
                    $this->authentication->on_hold === TRUE OR 
                    $this->authentication->current_hold_status()
                )
                ? 1 : 0;
                $isError = 1;


                echo json_encode(array(
                    'status'  => 0,
                    'count'   => $this->authentication->login_errors_count,
                    'on_hold' => $on_hold, 
                    'token'   => $this->tokens->token(),
                    'is_error' => $isError
                ));
            }
        }

        // Show 404 if not AJAX
        else
        {
            show_404();
        }
    }
    


	 public function register()
    {
        // Method should not be directly accessible

        if( strtolower( $_SERVER['REQUEST_METHOD'] ) == 'post' ){
            $this->create_user();

            
        }


        $this->setup_login_form();

        $html = $this->load->view('include/header.php', '', TRUE);
        $html .= $this->load->view('user/register.php', '', TRUE);
        $html .= $this->load->view('include/footer.php', '', TRUE);

        echo $html;
    }

	    // -----------------------------------------------------------------------

    /**
     * Most minimal user creation. You will of course make your
     * own interface for adding users, and you may even let users
     * register and create their own accounts.
     *
     * The password used in the $user_data array needs to meet the
     * following default strength requirements:
     *   - Must be at least 8 characters long
     *   - Must be at less than 72 characters long
     *   - Must have at least one digit
     *   - Must have at least one lower case letter
     *   - Must have at least one upper case letter
     *   - Must not have any space, tab, or other whitespace characters
     *   - No backslash, apostrophe or quote chars are allowed
     */
    public function create_user()
    {



        // Customize this array for your user
        $user_data = array(
            'passwd'     => $_POST['password'],
            'email'      => $_POST['email'],
            'auth_level' => '1', // 9 if you want to login @ examples/index.
        );

        $this->is_logged_in();

        // echo $this->load->view('examples/page_header', '', TRUE);

        // Load resources
        $this->load->model('examples_model');
        $this->load->model('validation_callables');
        $this->load->library('form_validation');

        $this->form_validation->set_data( $user_data );

        $validation_rules = array(
			array(
				'field' => 'username',
				'label' => 'username',
				'rules' => 'max_length[12]|is_unique[' . config_item('user_table') . '.username]',
                'errors' => array(
                    'is_unique' => 'Username already in use.'
                )
			),
			array(
				'field' => 'passwd',
				'label' => 'passwd',
				'rules' => array(
                    'trim',
                    'required',
                    array( 
                        '_check_password_strength', 
                        array( $this->validation_callables, '_check_password_strength' ) 
                    )
                ),
                'errors' => array(
                    'required' => 'The password field is required.'
                )
			),
			array(
                'field'  => 'email',
                'label'  => 'email',
                'rules'  => 'trim|required|valid_email|is_unique[' . config_item('user_table') . '.email]',
                'errors' => array(
                    'is_unique' => 'Email address already in use.'
                )
			),
			array(
				'field' => 'auth_level',
				'label' => 'auth_level',
				'rules' => 'required|integer|in_list[1,6,9]'
			)
		);

		$this->form_validation->set_rules( $validation_rules );

		if( $this->form_validation->run() )
		{
            $user_data['passwd']     = $this->authentication->hash_passwd($user_data['passwd']);
            $user_data['user_id']    = $this->examples_model->get_unused_id();
            $user_data['created_at'] = date('Y-m-d H:i:s');

            // If username is not used, it must be entered into the record as NULL
            if( empty( $user_data['username'] ) )
            {
                $user_data['username'] = NULL;
            }

			$this->db->set($user_data)
				->insert(config_item('user_table'));

			if( $this->db->affected_rows() == 1 ){
				$isError = 0;
                 echo json_encode(array(
                    'is_error' => $isError,
                    'success_text' => '<h1>Congratulations</h1>' . '<p>User ' . $user_data['username'] . ' was created.</p>',

                ));
            }
		}
		else
		{
            $isError = 1;
                echo json_encode(array(
                    'is_error' => $isError,
                    'error_text' => '<h1>User Creation Error(s)</h1>' . validation_errors(),

                ));
		}

    }

    public function booking()
    {
        $this->setup_login_form();  
        $this->load->view('include/header');
        
        if($this->auth_data){


            $data['listings'] = $this->Listing_Model->getBoookedListingsByUserId($this->auth_data->user_id);


            $this->load->view('management/booking/user-booking-list.php' , $data);
            $this->load->view('include/footer');

        }else{
            redirect('/');

        }


    }

    public function booking_single($id)
    {
        $this->setup_login_form();  
        $this->load->view('include/header');
        
        if($this->auth_data){


            $data['listings'] = $this->Listing_Model->getBoookedListingsByBookingId($id);


            $this->load->view('management/booking/user-booking-single.php' , $data);
            $this->load->view('include/footer');

        }else{
            redirect('/');

        }


    }


    public function management()
    {
        $this->setup_login_form();  
        $this->load->view('include/header');
        
        if($this->auth_data){

            $data['userdata'] = $this->Listing_Model->getUserDetails($this->auth_data->user_id);

            $this->load->view('management/user-details.php' , $data);
            $this->load->view('include/footer');

        }else{

            redirect('/');
        }


    }


    public function managementEdit()
    {
        $this->setup_login_form();  
        $this->load->view('include/header');
        
        if($this->auth_data){
            $data['userdata'] = $this->Listing_Model->getUserDetails($this->auth_data->user_id);
            $this->load->view('management/details/user-details-edit.php' , $data);
            $this->load->view('include/footer');

        }else{

            redirect('/');
        }


    }

   public function managementEditSubmit()
    {
        $data = $_POST;

        $this->setup_login_form();  

        $this->Listing_Model->addUserDetails($data,$this->auth_data->user_id);

        redirect('/user/management');


    }


    public function posting()
    {

        $this->setup_login_form();  
        $this->load->view('include/header');
        
        if($this->auth_data){


            $data['listings_active'] = $this->Listing_Model->getPostListingsByUserIdActive($this->auth_data->user_id);

            $data['listings_inactive'] = $this->Listing_Model->getPostListingsByUserIdInactive($this->auth_data->user_id);

            $data['listings_pending'] = $this->Listing_Model->getPostListingsByUserIdPending($this->auth_data->user_id);


            $this->load->view('management/posts/user-posts-list.php' , $data);
            $this->load->view('include/footer');

        }else{
            redirect('/');

        }

    }


    public function remove_listing($listing_id)
    {


            $this->Listing_Model->removeListingsByListingId($listing_id);

            redirect('/user/post');



    }

    public function approve($listing_id)
    {


        $this->setup_login_form();  
        $this->load->view('include/header');
        
        if($this->auth_data){

            $user_id = $this->Listing_Model->getUserIdByListingId($listing_id)->user_id ;
            $data['userdata'] = $this->Listing_Model->getUserDetails($user_id);

            $data['listing'] =  $this->Listing_Model->getUserIdByListingId($listing_id);

            $this->load->view('management/posts/user-approval.php' , $data);
            
            $this->load->view('include/footer');

        }else{

            redirect('/');
        }

    }

    public function approveUser($listing_id)
    {

        $this->Listing_Model->removeListingsByListingId($listing_id);

        redirect('/user/post');
    
    }

    public function repostDisplay($id){

        $this->setup_login_form();

        if($this->auth_data){

            $data['listings'] = $this->Listing_Model->getListingImageById($id);
            $this->load->view('include/header');
            $this->load->view('management/user-repost.php',$data);
            $this->load->view('include/footer');

        }else{

            // redirect('/');

        }
    }

    public function repost(){


        $data = $_POST;


        $this->Listing_Model->repostChangeAvailbility($data);
        $this->Listing_Model->setListingActive($data['listing_id']);

        echo $data['listing_id'];
        redirect('/user/post/', 'refresh');



    }


        /**
     * Show any login error message.
     *
     * @param  bool  if login is optional or required
     */
    protected function setup_login_form( $optional_login = FALSE )
    {

        $this->is_logged_in();

        if( $this->auth_data ){
            $view_data['is_logged_in'] = TRUE;
            $view_data['auth_data'] = $this->auth_data;
        }else
            $view_data['is_logged_in'] = FALSE;


        $this->tokens->name = 'login_token';

        /**
         * Check if IP, username, or email address on hold.
         *
         * If a malicious form post set the on_hold authentication class 
         * member to TRUE, there'd be no reason to continue. Keep in mind that 
         * since an IP address may legitimately change, we shouldn't do anything 
         * drastic unless this happens more than an acceptable amount of times.
         * See the 'deny_access_at' config setting in config/authentication.php
         */
        if( $this->authentication->on_hold === TRUE )
        {
            $view_data['on_hold_message'] = 1;
        }

        // This check for on hold is for normal login attempts
        else if( $on_hold = $this->authentication->current_hold_status() )
        {
            $view_data['on_hold_message'] = 1;
        }


        // Display a login error message if there was a form post
        if( $this->authentication->login_error === TRUE )
        {
            // Display a failed login attempt message
            $view_data['login_error_mesg'] = 1;
        }

        // Redirect to specified page
        $redirect = $this->input->get('redirect')
            ? '?redirect=' . $this->input->get('redirect') 
            : '?redirect=' . config_item('default_login_redirect');

        // If optional login, redirect to optional login's page
        if( $optional_login )
        {
            $redirect = '?redirect=' . urlencode( $this->uri->uri_string() );

            $view_data['optional_login'] = TRUE;
        }

        // Set the link protocol
        $link_protocol = USE_SSL ? 'https' : NULL;

        // Load URL helper for site_url function
        $this->load->helper('url');

        // Set the login URL
        $view_data['login_url'] = site_url( LOGIN_PAGE . $redirect, $link_protocol );

        $this->load->vars( $view_data );
    }




}

/* End of file User.php */
/* Location: ./application/controllers/User.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
    $this->load->model('Listing_Model');
	}

	public function index()
	{
		$this->setup_login_form();	
		$this->load->view('include/header');
		
		if($this->auth_data){

			$this->load->view('home/login_landing.php');
			$this->load->view('include/footer');

		}else{
			$this->load->model('Testimonial_modal');
			$data['testmonials'] = $this->Testimonial_modal->getAllTestimonals();
			$this->load->view('home/landing_page.php',$data);
			$this->load->view('include/footer');

		}

		
	
	}

    public function faq()
  {
    $this->setup_login_form();  


    $this->load->view('include/header.php');
    $this->load->view('home/contact.php');
    $this->load->view('include/footer.php');
  }

  public function createPost()
  {

        $this->setup_login_form();  
        $this->load->view('include/header');

		if($this->auth_data){

			 $this->load->view('post/create.php');
			$this->load->view('include/footer');

		}else{
			$this->load->model('Testimonial_modal');
			$data['testmonials'] = $this->Testimonial_modal->getAllTestimonals();
			$this->load->view('home/landing_page.php',$data);
			$this->load->view('include/footer');
			redirect('/');

		}

  }
   public function postList()
  {

        $this->setup_login_form();  
        $this->load->view('include/header');

        $data =array( 
        	'location' => "",
        	'from'=>date('Y-m-d'),
        	'to'=>""
        	 );


           $data['listings'] = $this->Listing_Model->getListingandALLByWithDate($data); 


          
          $this->load->view('post/list.php',$data);
          $this->load->view('include/footer');


  }
  public function postListsearch()
  {

  		$data = $_POST;



  		if (empty($data)) {
			$data =array( 
        		'location' => "",
        		'from'=>date('Y-m-d'),
        		'to'=>""
        	);

		}

        $this->setup_login_form();  
        $this->load->view('include/header');
	    $data['listings'] = $this->Listing_Model->getListingandALLByWithDate($data); 
	    $this->load->view('post/list.php',$data);
	    $this->load->view('include/footer');


  }
  public function postSingle($id)
  {

        $this->setup_login_form();  
        $data['listings'] = $this->Listing_Model->getListingImageById ($id);
        $this->load->view('include/header');
        $this->load->view('post/single.php',$data);
        $this->load->view('include/footer');

    
  }

  public function bookDisplay($id){

        $this->setup_login_form();

		if($this->auth_data){

	        $data['listings'] = $this->Listing_Model->getListingImageById($id);
	        $this->load->view('include/header');
	        $this->load->view('booking/single.php',$data);
	        $this->load->view('include/footer');

		}else{

			redirect('/');

		}


  }

public function book($data){


	$data = $_POST;


    $this->setup_login_form();  
    $this->Listing_Model->book($data);


    $this->Listing_Model->setListingPending($data['listing_id']);

    // $data['book'] = $this->Listing_Model->adjustStatus($data);
    redirect('/post/list/', 'refresh');



}



  public function createPostDatabaseEntry()
  {


      $data = $_POST;

      echo json_encode($this->Listing_Model->createListing($data));
       


  }

  public function uploadPostImages()
  {
 		$ds = DIRECTORY_SEPARATOR;  //1
 
		$storeFolder = 'assets/images/postings';   //2

		 
		if (!empty($_FILES)) {
			//print_r($_FILES);
		     
		    $tempFile = $_FILES['file']['tmp_name'][0];          //3             
		      
		    $targetPath = FCPATH . $ds. $storeFolder . $ds;  //4
		     
		    $targetFile =  $targetPath. $_FILES['file']['name'][0];  //5
		 
		    move_uploaded_file($tempFile,$targetFile); //6
		}

    $imagefile_storeFolder = site_url().'assets/images/postings/'.$_FILES['file']['name'][0];   //2
		
    $this->Listing_Model->setListingImage($_POST['hidden_listing_id'] , $imagefile_storeFolder );

    redirect('/', 'refresh');


  }



	/**
	 * Show any login error message.
	 *
	 * @param  bool  if login is optional or required
	 */
	protected function setup_login_form( $optional_login = FALSE )
	{

		$this->is_logged_in();

		if( $this->auth_data ){
			$view_data['is_logged_in'] = TRUE;
			$view_data['auth_data'] = $this->auth_data;
		}else
			$view_data['is_logged_in'] = FALSE;


		$this->tokens->name = 'login_token';

		/**
		 * Check if IP, username, or email address on hold.
		 *
		 * If a malicious form post set the on_hold authentication class 
		 * member to TRUE, there'd be no reason to continue. Keep in mind that 
		 * since an IP address may legitimately change, we shouldn't do anything 
		 * drastic unless this happens more than an acceptable amount of times.
		 * See the 'deny_access_at' config setting in config/authentication.php
		 */
		if( $this->authentication->on_hold === TRUE )
		{
			$view_data['on_hold_message'] = 1;
		}

		// This check for on hold is for normal login attempts
		else if( $on_hold = $this->authentication->current_hold_status() )
		{
			$view_data['on_hold_message'] = 1;
		}


		// Display a login error message if there was a form post
		if( $this->authentication->login_error === TRUE )
		{
			// Display a failed login attempt message
			$view_data['login_error_mesg'] = 1;
		}

		// Redirect to specified page
		$redirect = $this->input->get('redirect')
			? '?redirect=' . $this->input->get('redirect') 
			: '?redirect=' . config_item('default_login_redirect');

		// If optional login, redirect to optional login's page
		if( $optional_login )
		{
			$redirect = '?redirect=' . urlencode( $this->uri->uri_string() );

			$view_data['optional_login'] = TRUE;
		}

		// Set the link protocol
		$link_protocol = USE_SSL ? 'https' : NULL;

		// Load URL helper for site_url function
		$this->load->helper('url');

		// Set the login URL
		$view_data['login_url'] = site_url( LOGIN_PAGE . $redirect, $link_protocol );

		$this->load->vars( $view_data );
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */
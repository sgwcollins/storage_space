<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Listing_Model extends CI_Model {



  public function __construct()

  {

    parent::__construct();

    $this->load->database();



    

  }





  public function createListing($data)

  {


    $availability_insert_date = array(

      'to' => date('Y-m-d',strtotime($data['to'])),
      'from' => date('Y-m-d',strtotime($data['from']))

     );

    $availability_insert = $this->db->insert('availability', $availability_insert_date);


    $availability_insert_id = $this->db->insert_id();


    $listing_insert_data = array(

      'name' => $data['space-name'],

      'space-description' => $data['space-description'],

      'address' => $data['address'],

      'city' => $data['city'],

      'price' => $data['price'], 

      'availability_id' => $availability_insert_id,

      'type' => $data['type'], 

      'dimension' => $data['dimensions'],

      'createdDate' => date("Y/m/d") ,

      'daily_price' => (17.5 / 100) * intval($data['price']) ,

      'user_id' => $data['user_id'] 

    );



    $insert = $this->db->insert('listing', $listing_insert_data);
    
    $insert_id = $this->db->insert_id();



    $this->db->where('id', $availability_insert_id);

    $this->db->update('availability', array('listing_id' => $insert_id));

    return $insert_id;

 

  }

  public function repostChangeAvailbility($data)

  {



    $availability_update_data = array(

        'from' => date('Y-m-d',strtotime($data['book-in'])),
        'to' => date('Y-m-d',strtotime($data['book-out']))

    );

   $this->db->where('listing_id', $data['listing_id']);

   $this->db->update('availability', $availability_update_data);





  }




public function setListingImage( $listing_id , $image)

  {

    

    $listing_insert_data = array(

      'listing_id' => $listing_id ,

      'image_url' => $image      

    );



    $insert = $this->db->insert('listing_images', $listing_insert_data);


  }


  public function setListingActive($listing_id)

  {

    

    $listing_update_data = array(

      'status' => 'active'

    );

   $this->db->where('id', $listing_id);

   $this->db->update('listing', $listing_update_data);


  }

  public function setListingPending($listing_id)

  {

    

    $listing_update_data = array(

      'status' => 'pending'

    );

   $this->db->where('id', $listing_id);

   $this->db->update('listing', $listing_update_data);


  }




public function getUserDetails($user_id)

  {

    

      $this->db->select('*');

      $this->db->from('user-details');

      $this->db->where('user_id', $user_id);

      $query = $this->db->get();

      return $query->row();

  }

  public function addUserDetails($data,$user_id)

  {


      $this->db->select('*');

      $this->db->from('users');

      $this->db->where('user_id', $user_id);

      $query = $this->db->get();


      if($query->row()){

            $user_insert_data = array(

              'first_name' => $data['first_name'] ,

              'last_name' => $data['last_name'],

              'occupation' => $data['occupation'],

              'about' => $data['about'],

              'hobbies' => $data['hobbies'],

            );

           $this->db->where('user_id', $user_id);

           $this->db->update('user-details', $user_insert_data);

      }else {


         $user_insert_data = array(

          'first_name' => $data['first_name'] ,

          'last_name' => $data['last_name'],

          'occupation' => $data['occupation'],

          'about' => $data['about'],

          'hobbies' => $data['hobbies'],

          'user_id' => $user_id,


        );


        $insert = $this->db->insert('user-details', $user_insert_data);

      }





      $this->db->select('*');

      $this->db->from('user-details');

      $this->db->where('user_id', $user_id);

      $query = $this->db->get();

      return $query->row();

  }

  public function getUserIdByListingId($listing_id)

  {

      $this->db->select('*');

      $this->db->from('listing');

      $this->db->where('listing.id', $listing_id);

      $query = $this->db->get();

      return $query->row();

  }


public function getListingImageById($listing_id)

  {

    

      $this->db->select('*');

      $this->db->from('listing');

      $this->db->where('listing.id', $listing_id);

      $this->db->join('availability', 'listing.id = availability.listing_id','right outer');

      $query = $this->db->get();

      return $query->row();

  }


  public function getPostListingsByUserIdActive($user_id)

  {

      $this->db->select('*');

      $this->db->from('listing');

      $this->db->join('booking', 'listing.id = booking.listing_id', 'left');

      $this->db->join('listing_images', 'listing.id = listing_images.listing_id','left');

      $this->db->join('availability', 'listing.id = availability.listing_id','left');

            $this->db->group_by('listing.id');

      $this->db->where('listing.user_id', $user_id);

      $this->db->where('listing.status', 'active');

      $query = $this->db->get();

      return $query->result_array();

  }


  public function getPostListingsByUserIdInactive($user_id)
  {

      $this->db->select('*');


      $this->db->from('listing');

      $this->db->join('booking', 'listing.id = booking.listing_id', 'left');

      $this->db->join('listing_images', 'listing.id = listing_images.listing_id','left');



      $this->db->join('availability', 'listing.id = availability.listing_id','left');

      $this->db->group_by('listing.id');

      $this->db->where('listing.user_id', $user_id);

      $this->db->where('listing.status', 'inactive');

      $query = $this->db->get();

      return $query->result_array();

  }


  public function getPostListingsByUserIdPending($user_id)
  {

      $this->db->select('*');

      $this->db->distinct();

      $this->db->from('listing');

      $this->db->join('booking', 'listing.id = booking.listing_id', 'left');

      $this->db->join('listing_images', 'listing.id = listing_images.listing_id','left');

      $this->db->join('availability', 'listing.id = availability.listing_id','left');

            $this->db->group_by('listing.id');

      $this->db->where('listing.user_id', $user_id);

      $this->db->where('listing.status', 'pending');

      $query = $this->db->get();

      return $query->result_array();

  }

    public function removeListingsByListingId($listing_id){



    $listing_update_data = array(

      'status' => 'inactive'

    );

   $this->db->where('id', $listing_id);

   $this->db->update('listing', $listing_update_data);


  }

  

  public function getBoookedListingsByUserId($user_id)

  {

      $this->db->select('*');

      $this->db->from('listing');

      $this->db->join('booking', 'listing.id = booking.listing_id', 'left');

      $this->db->join('listing_images', 'listing.id = listing_images.listing_id','outer left');

      $this->db->group_by('listing.id');

      $this->db->where('booking.user_id', $user_id);

      $query = $this->db->get();

      return $query->result_array();

  }



    public function getBoookedListingsByBookingId($id)

  {

      $this->db->select('*');

      $this->db->from('booking');

      $this->db->where('booking.booking_id', $id);

      $this->db->join('listing', 'listing.id = booking.listing_id','left');

      $this->db->join('listing_images', 'listing.id = listing_images.listing_id','left outer');

      $query = $this->db->get();

      return $query->row();

  }



  public function book($data){



      $bookIn = date('Y-m-d',strtotime($data['book-in']));

      $bookOut = date('Y-m-d',strtotime($data['book-out']));

      $booking_insert_data = array(

      'listing_id' => $data['listing_id'] ,

      'booked_from' => $bookIn ,

      'booked_to' => $bookOut ,

      'user_id' => $data['user_id'],

      'total_days' => $data['total_days'],

      'total_price' => $data['total_price']

    );


    $insert = $this->db->insert('booking', $booking_insert_data);

    $listing_update_data = array(

      'status' => 'pending'

    );

   $this->db->where('id', $data['listing_id']);

   $this->db->update('listing', $listing_update_data);


  }


  public function adjustAvailability($data){



      $bookOut = date('Y-m-d',strtotime($data['book-out']));



      $booking_update_data = array(

      'from' => $bookOut

    );

   $this->db->where('listing_id', $data['listing_id']);

   $this->db->update('availability', $booking_update_data);


  }

  public function getListingandALLBy()

  {

    

     

      $this->db->select('*');

      $this->db->from('listing');

      $this->db->join('listing_images', 'listing.id = listing_images.listing_id','left outer');

      $this->db->join('availability', 'listing.id = availability.listing_id','left outer');

      $this->db->where('availability.from >=', $newFrom );

      $query = $this->db->get();

      return $query->result_array();


  }

  
  public function getListingandALLByWithDate($data)

  {

    


      $location = $data['location'];

      $newFrom = date('Y-m-d',strtotime($data['from']));


     
      $this->db->select('*');

      $this->db->from('listing');

      $this->db->join('listing_images', 'listing.id = listing_images.listing_id','left');

      $this->db->join('availability', 'listing.id = availability.listing_id','left');


      if($newFrom!=""){
        $this->db->where('availability.from >=', $newFrom );
      }

      if($location!=""){

        $this->db->where("listing.city LIKE '%$location%' OR listing.address LIKE '%$location%'");
      }


      $this->db->where('listing.status', 'active');

      $query = $this->db->get();

      return $query->result_array();

  }




public function getListingAndImage()

  {

    
      $this->db->select('*');

      $this->db->distinct();

      $this->db->from('listing');

      $this->db->join('listing_images', 'listing.id = listing_images.listing_id','left');

      $query = $this->db->get();

      return $query->result_array();    

  }

}





/* End of file Listing_Model.php */

/* Location: ./application/models/Listing_Model.php */
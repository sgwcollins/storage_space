<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


 $route['login'] = 'user/login';
 $route['register'] = 'user/create_user';
 $route['logout'] = 'user/logout';

 $route['faq'] = 'home/faq';

 $route['bookDisplay/(:any)'] = 'home/bookDisplay/$1';
 $route['post/book'] = 'home/book/';

 $route['post/create'] = 'home/createPost';
 $route['post/createPostDB'] = 'home/createPostDatabaseEntry';
 $route['post/upload'] = 'home/uploadPostImages';
 $route['post/list'] = 'home/postList';
 $route['post/list/search'] = 'home/postListsearch';
 $route['post/list/(:any)'] = 'home/postSingle/$1';



  $route['user/approve/(:any)'] = 'user/approve/$1';
  $route['user/approveUser/(:any)'] = 'user/approveUser/$1';
  

  $route['user/booking'] = 'user/booking';
  $route['user/booking/single/(:any)'] = 'user/booking_single/$1';



  $route['user/management'] = 'user/management';
  $route['user/managementEdit'] = 'user/managementEdit';
  $route['user/managementEditSubmit'] = 'user/managementEditSubmit';


  $route['user/repost'] = 'user/repost';
  $route['user/repostDisplay/(:any)'] = 'user/repostDisplay/$1';



  $route['user/post'] = 'user/posting';
  $route['user/remove_listing/(:any)'] = 'user/remove_listing/$1';

 

 $route['testimonals'] = 'testimonal';
 $route['testimonals/(:any)'] = 'testimonal/single/$1';

 $route['assets/(:any)'] = 'assets/$1';

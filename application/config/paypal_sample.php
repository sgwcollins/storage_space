<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting PayPal's sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */
$config['Sandbox'] = TRUE;

/* 
 * PayPal API Version
 * ------------------
 * The library is currently using PayPal API version 123.0.
 * You may adjust this value here and then pass it into the PayPal object when you create it within your scripts to override if necessary.
 */
$config['APIVersion'] = '204';

/*
 * PayPal Gateway API Credentials
 * ------------------------------
 * These are your PayPal API credentials for working with the PayPal gateway directly.
 * These are used any time you're using the parent PayPal class within the library.
 * 
 * We're using shorthand if/else statements here to set both Sandbox and Production values.
 * Your sandbox values go on the left and your live values go on the right.
 * 
 * You may obtain these credentials by logging into the following with your PayPal account.
 * Sandbox: https://www.sandbox.paypal.com/us/cgi-bin/webscr?cmd=_login-api-run
 * Live: https://www.paypal.com/us/cgi-bin/webscr?cmd=_login-api-run
 *
 */
$config['APIUsername'] = $config['Sandbox'] ? 'sgwcollins_api1.gmail.com' : 'sgwcollins_api1.gmail.com';
$config['APIPassword'] = $config['Sandbox'] ? 'LXYTJM4CZ3M3YXLE' : 'LXYTJM4CZ3M3YXLE';
$config['APISignature'] = $config['Sandbox'] ? 'AFcWxV21C7fd0v3bYYYRCpSSRl31ALzYGdd6HY58UVfUaFBqytceNDlC' : 'AFcWxV21C7fd0v3bYYYRCpSSRl31ALzYGdd6HY58UVfUaFBqytceNDlC';


/*
 * PayPal Developer Account Email Address
 * This is the email address that you use to sign in to http://developer.paypal.com
 */
$config['DeveloperEmailAccount'] = 'sgwcollins@gmail.com';

/**
 * Third Party User Values
 * These can be setup here or within each caller directly when setting up the PayPal object.
 */


/* End of file paypal.php */
/* Location: ./system/application/config/paypal.php */
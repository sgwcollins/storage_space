<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonial_modal extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	function getAllTestimonals ()
    {
        $query = $this->db->get('testimonials');
        return $query->result_array();
    }
    function getTestimonalsById($id)
    {
          $this->db->select('*');
	      $this->db->from('testimonials');
	      $this->db->where('id', $id);
	      $query = $this->db->get();

        return $query->row();
    }


	

}

/* End of file  */
/* Location: ./application/models/ */